// alert(`hello`)


let courses = [
	{
		id: '1',
		name: 'HTML',
		description:'new description',
		price: 1500,
		isActive: true
	},
	{
		id: '2',
		name: 'JavaScript',
		description:'new description',
		price: 2500,
		isActive: true
	},
	{
		id: '3',
		name: 'Python',
		description:'new description',
		price: 3500,
		isActive: true
	},
	{
		id: '4',
		name: 'React.js',
		description:'new description',
		price: 4500,
		isActive: true
	}
]

//CREATE
let addCourse = (id, name, desc, price, isActive) => {
	let course = {};
	course.id =id;
	course.name = name;
	course.desc = desc;
	course.price = price;
	course.isActive = isActive;
	courses.push(course)
	alert(`You have created ${course.name}. It's price is ${course.price}.`)
	return courses;
}
//addCourse("5","Next.js","Just a description", 2500, true);

//UPDATE
let archiveCourse =(index) => {
	courses[index].isActive = false;
	console.log(`${courses[index].name} is updated to ${courses[index].isActive}.`)
	console.log(courses[index])
}
archiveCourse(2);


//DELETE
let deleteCourse = () =>  {
	let deleteItem = courses.pop()
	console.log(courses)
}
// deleteCourse();

let getSingleCourse = (id) => {
	let foundCourse = courses.find((element) => {
		return element.id ==id;
	})
	console.log(foundCourse)
}
// getSingleCourse(1);

let getAllCourses = () => {
	let foundAllCourses = courses.forEach((element) => {
		console.log(element)
	})
}
// getAllCourses()


let getActive = (isActive) => {
	let activeCourse = courses.filter((element)=> {
		return element.isActive == true;
	})
	console.log(activeCourse)
}
getActive()
